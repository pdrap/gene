#pragma once

#include <shared_mutex>

class Threshold {
    float th;
    std::shared_mutex mu;

  public:
    Threshold(void);
    ~Threshold(void) = default;

    // Get the current threshhold
    float get(void);

    // Losers below the threshhold update this
    void set(float newscore);
};
