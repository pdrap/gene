#pragma once

#include <atomic>
#include <functional>
#include <string>
#include <thread>
#include <vector>

#include "gene.h"
#include "threshold.h"

class Population;

class Chromosome {
    std::vector<Gene> genes;

    std::shared_ptr<std::thread> thread;
    std::shared_mutex chr_mutex;
    std::shared_mutex gen_mutex;

    bool done;

    Threshold &threshold;

    std::atomic_uint score;
    std::atomic_ulong generation;

    void thread_main(std::string pattern);

    std::function<std::vector<Gene>(void)> &crossover_function;

  public:
    Chromosome(Threshold &threshold, std::function<std::vector<Gene>(void)> &crossover_function);
    Chromosome(const Chromosome &) = default;
    ~Chromosome(void) = default;

    unsigned score_gene(std::string &pattern);
    void mutate_a_percentage(unsigned percent);
    void kill_the_weak(unsigned score, unsigned percent);

    void run_chromosome(std::string pattern);
    void terminate(void);

    std::string to_string(void);
    unsigned long get_generations(void);

    std::vector<Gene> get_genes(void);
};
