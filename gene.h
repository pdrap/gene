#pragma once

struct Gene {
    char emit;
    unsigned int nextstate;

    Gene(void) = default;
    Gene(const Gene &) = default;
    ~Gene(void) = default;

    void randomize(unsigned chromosome_size);
};
