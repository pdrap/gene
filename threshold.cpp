#include <mutex>
#include <shared_mutex>

#include "threshold.h"

Threshold::Threshold(void) : th(3.0), mu() {
}

float Threshold::get(void) {
    std::shared_lock<std::shared_mutex> lock(mu);
    return th;
}

void Threshold::set(float newscore) {
    std::unique_lock<std::shared_mutex> lock(mu);

    th = (newscore - th) * (2.0 / 5000.0) + th;

    // th = th - (th / 5);
    // th = th + newscore / 5;
}
