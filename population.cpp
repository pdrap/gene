#include <algorithm>
#include <chrono>
#include <functional>
#include <iomanip>
#include <iostream>
#include <ncurses.h>
#include <sstream>
#include <string>
#include <vector>

#include "chromosome.h"
#include "gene.h"
#include "population.h"
#include "thread_random.h"

Population::Population(const unsigned population_size)
    : display_thread_terminate(false), crossover_function(std::bind(&Population::crossover, this)) {

    for (unsigned i = 0; i < population_size; i++) {
        std::shared_ptr<Chromosome> new_chromosome = std::make_shared<Chromosome>(threshold, crossover_function);
        chromosomes.push_back(new_chromosome);
    }
}

std::vector<Gene> Population::crossover(void) {
    if (chromosomes.size() > 1) {
        std::shared_ptr<Chromosome> a = chromosomes[unsigned_rand(0, chromosomes.size() - 1)];
        std::shared_ptr<Chromosome> b;
        do {
            b = chromosomes[unsigned_rand(0, chromosomes.size() - 1)];
        } while (a != b);

        std::vector<Gene> ga = a->get_genes();
        std::vector<Gene> gb = a->get_genes();

        int crossover = unsigned_rand(0, std::min(ga.size(), gb.size()) - 1);

        std::vector<Gene> new_genes;

        std::copy(ga.begin(), ga.begin() + crossover, std::back_inserter(new_genes));
        std::copy(gb.begin() + crossover, gb.end(), std::back_inserter(new_genes));
        return new_genes;
    }
    return std::vector<Gene>();
}

void Population::go(std::string &ppattern) {
    pattern = ppattern;

    // start up the chromosome threads
    for (std::shared_ptr<Chromosome> &chromosome : chromosomes) {
        chromosome->run_chromosome(pattern);
    }

    // start up the display thread
    display_thread = std::make_shared<std::thread>([this]() { this->do_display(); });

    start_time = std::chrono::steady_clock::now();
}

void Population::do_display(void) {
    unsigned long generations;

    while (!display_thread_terminate) {
        generations = 0;

        for (unsigned i = 0; i < chromosomes.size(); i++) {
            move(i + 4, 0);
            addstr(chromosomes[i]->to_string().c_str());

            generations += chromosomes[i]->get_generations();
        }
        {
            std::stringstream s;
            s << "Generations " << generations << "             ";
            mvprintw(0, 0, "%s", s.str().c_str());
        }
        {
            std::stringstream s;
            s << "Pattern Length " << pattern.length() << "             ";
            mvprintw(0, 30, "%s", s.str().c_str());
        }
        mvprintw(1, 0, "Threshold %f      ", threshold.get());
        refresh();

        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }
}

void Population::terminate(void) {
    for (auto &chromosome : chromosomes) {
        chromosome->terminate();
    }
    display_thread_terminate = true;
    display_thread->join();
}

void Population::print_final_stats(void) {
    // just print the final value for benchmarking
    unsigned long generations = 0;
    std::chrono::duration<double> diff = std::chrono::steady_clock::now() - start_time;

    for (unsigned i = 0; i < chromosomes.size(); i++) {
        generations += chromosomes[i]->get_generations();
    }

    std::cout << std::endl
              << "Number of threads " << chromosomes.size() << std::endl
              << "Total generations " << generations << std::endl
              << "Generations per thread per milliseconds "
              << generations / chromosomes.size() / std::chrono::duration_cast<std::chrono::milliseconds>(diff).count()
              << std::endl
              << "Total generations per millisecond "
              << generations / chromosomes.size() /
                     std::chrono::duration_cast<std::chrono::milliseconds>(diff).count() * chromosomes.size()
              << std::endl
              << "Total runtime in seconds " << std::chrono::duration_cast<std::chrono::seconds>(diff).count()
              << std::endl
              << std::endl;
}
