#PROFILING_COMPILER=-pg
#PROFILING_LINKER=-pg
CXXFLAGS=-Wall -Wextra -pedantic-errors -std=c++17 -O3 -g $(PROFILING_COMPILER)


OBJS=main.o ship.o chromosome.o population.o gene.o thread_random.o threshold.o

LDFLAGS= -Wl,-Bdynamic $(PROFILING_LINKER)
LIBS =-lstdc++ -lncurses -lpthread

PROG=main

all:	$(PROG)
	@date
	@echo make all complete

$(PROG): $(OBJS)
	g++ $(LDFLAGS) $(OBJS) -o main $(LIBS)

clean:
	rm -rf core *.o $(PROG)
	@echo make clean complete
	
depend:
	for i in $(shell find . -name "*.cpp" -print); do $(CC) $(FLAGS) -MM $$i; done > .depend
	@echo make depend complete

#######################################################################
# dependency includer below
#######################################################################

ifeq (.depend,$(wildcard .depend))
include .depend
endif
# DO NOT DELETE


	
