#include <algorithm>
#include <cstdlib>
#include <functional>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <thread>
#include <vector>

#include "chromosome.h"
#include "thread_random.h"
#include "threshold.h"

Chromosome::Chromosome(Threshold &threshold, std::function<std::vector<Gene>(void)> &crossover_function)
    : thread(nullptr), done(false), threshold(threshold), score(0), generation(0),
      crossover_function(crossover_function) {

    std::unique_lock<std::shared_mutex> lock(chr_mutex);

    Gene gene;
    gene.randomize(1);

    genes.push_back(gene);
}

unsigned Chromosome::score_gene(std::string &pattern) {
    unsigned state = 0;
    unsigned score = 0;

    std::shared_lock<std::shared_mutex> lock(chr_mutex);
    for (auto &ch : pattern) {
        if (genes[state].emit != ch) {
            break;
        }
        score++;
        state = genes[state].nextstate;
    }
    return score;
}

void Chromosome::mutate_a_percentage(unsigned percent) {
    if (unsigned_rand(0, 99) < percent) {
        std::unique_lock<std::shared_mutex> lock(chr_mutex);
        genes[unsigned_rand(0, genes.size() - 1)].randomize(genes.size());

        if (unsigned_rand(0, 999) <= 1) { // 1 in 500 of the mutated grow a random gene

            Gene gene;
            gene.randomize(genes.size());

            genes.push_back(gene);
        }
    }
}

void Chromosome::kill_the_weak(unsigned score, unsigned percent) {
    threshold.set(score);
    if (score - 0.001 <= threshold.get()) { // if this one is weak

        if (unsigned_rand(0, 199) < percent) { // and kill half a percentage of the weak
            // crossover two randoms to replace
            std::vector<Gene> newgenes = crossover_function();
            if (newgenes.size() > 0) {
                std::unique_lock<std::shared_mutex> lock(chr_mutex);
                genes = newgenes;
            }
        }

        mutate_a_percentage(10);
    }
}

void Chromosome::thread_main(std::string pattern) {
    int loopcount = 0;
    int lscore = 0;
    while (!done) {
        lscore = score_gene(pattern);

        kill_the_weak(lscore, 10);

        const int LOOPS = 20000;
        if (++loopcount == LOOPS) {
            loopcount = 0;
            std::atomic_fetch_add(&generation, LOOPS);
            score = lscore;
        }
    }
    std::atomic_fetch_add(&generation, loopcount);
    score = lscore;
}

void Chromosome::run_chromosome(std::string pattern) {
    thread = std::make_shared<std::thread>([this, pattern]() { this->thread_main(pattern); });
}

void Chromosome::terminate(void) {
    done = true;
    thread->join();
}
std::string Chromosome::to_string(void) {
    std::stringstream s;

    std::shared_lock<std::shared_mutex> lock(chr_mutex);
    s << std::left << std::setw(7) << score.load() << std::left << std::setw(7) << genes.size();

    return s.str();
}

unsigned long Chromosome::get_generations(void) {
    return generation;
}

std::vector<Gene> Chromosome::get_genes(void) {
    std::shared_lock<std::shared_mutex> lock(chr_mutex);
    return genes;
}
