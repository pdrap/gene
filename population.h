#pragma once

#include <chrono>
#include <functional>
#include <memory>
#include <thread>
#include <vector>

#include "chromosome.h"
#include "gene.h"
#include "threshold.h"

class Chromosome;

class Population {
    Threshold threshold;
    std::vector<std::shared_ptr<Chromosome>> chromosomes;
    std::shared_ptr<std::thread> display_thread;
    bool display_thread_terminate;
    std::chrono::steady_clock::time_point start_time;
    std::function<std::vector<Gene>(void)> crossover_function;

    std::string pattern;

  public:
    Population(void) = delete;
    Population(unsigned population_size);
    ~Population(void) = default;
    Population(const Population &) = delete;

    void go(std::string &pattern);
    void do_display(void);

    std::vector<Gene> crossover(void);

    void terminate(void);
    void print_final_stats(void);
};
