
//#include <algorithm>
//#include <clocale>
//#include <cstdlib>
//#include <ctime>
#include <iostream>
#include <ncurses.h>
#include <string>

#include "population.h"

int main(int argc, char **argv) {
    // init curses
    initscr();
    clear();
    cbreak();
    noecho();

    mvprintw(3, 0, "Score  Length");
    unsigned POPULATION_SIZE;
    if (argc < 2) {
        POPULATION_SIZE = 1;
    } else {
        POPULATION_SIZE = atoi(argv[1]);
    }
    Population population(POPULATION_SIZE);
    std::string pattern = "111001010100100101101001000011001001001010110101001";

    population.go(pattern);

    getch();
    // send terminate to all threads
    population.terminate();

    getch();

    endwin();

    population.print_final_stats();

    return 0;
}
