#include <random>

#include "thread_random.h"

unsigned unsigned_rand(const unsigned &min, const unsigned &max) {
    static thread_local std::mt19937 generator;
    std::uniform_int_distribution<unsigned> distribution(min, max);
    return distribution(generator);
}
