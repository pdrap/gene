#include "gene.h"
#include "thread_random.h"

void Gene::randomize(unsigned chromosome_size) {
    emit = (char)unsigned_rand(0, 1) + (char)'0';
    nextstate = unsigned_rand(0, chromosome_size - 1);
}
